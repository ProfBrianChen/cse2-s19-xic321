//Hailey Cai, CSE2,
//lab09
//method1: random generate array 
//method2: random generate ascending integers
//method3: linear search
//method4: accecpt(arr[], int a) binary search 
//main perform either binary or linear search
//main: if binary(b)-d
// .    if linear(a)-c


import java.util.*;         
public class Searching{
  public static int[] a(int arrSize ){ //method a. give a random array
    int arr[] = new int[arrSize];
    for(int i=0;i<arr.length;i++){ 
      arr[i]=(int)(Math.random()*arrSize+1);
    }
    
    
    return arr;
  } //end a
  
  public static int[] b(int arrSize){ //method b , give a random array with ascending 
     int arr[] = new int [arrSize];
     int temp=0;
     for(int i=0;i<arr.length;i++){ 
      arr[i]=(int)(Math.random()*arrSize+1);
    }
      Arrays.sort(arr);
    
   return arr;
  }//end b
  
  public static int c(int[] arr, int num){ //linear
    
    for(int i=0;i<arr.length;i++){
      if(arr[i]==num){
        return i;   //i is the index of the integer num
      }
    }
    
    return -1;
  }   
  
  public static int d(int[] arr, int num){ //binary
   int min=0; 
   int max=arr.length-1;
   int mid=0;
   
   while(arr[mid]!=num){
     mid= (max+min)/2;
     if(arr[mid]<num){
       min=mid+1;
     }
     if(arr[mid]>num){
       max=mid-1;
     }
     if(min>=max){
       return -1;
     }
   }
   return mid;
  }   
  
  public static void main(String[] args){
 
    Scanner myScanner = new Scanner(System.in);
    System.out.print("give a size the of array: ");
    int arrSize = myScanner.nextInt();
    System.out.print("what integer do you want to search: ");
    int num=myScanner.nextInt();
    System.out.print("type in 'linear' or 'binary' to search the integer: ");
    //use if statemetn to ask equals....string 
    String userInput = myScanner.next();
    
    
    if(userInput.equals("linear")){
      int [] Arraya = a(arrSize);
      int found1 = c(Arraya, num );
      System.out.print("the array is ");
      for(int i=0;i<Arraya.length;i++){
          System.out.print(Arraya[i]+" ");
      }
            System.out.println();

      if(found1 !=-1){
        System.out.println("the index of the array is: "+found1);//dsdfwfwdfr
      }
      else{
        System.out.println("result cannot be found");
      }
     
    }//a-c linear search
    if(userInput.equals("binary")){
      int[] Arrayb =b(arrSize);
      int found2= d(Arrayb, num);
      System.out.print("the array is ");
      for(int i=0;i<Arrayb.length;i++){
          System.out.print(Arrayb[i]+" ");

      }
      System.out.println();
      if(found2 !=-1){
        System.out.println("the index of the array is: "+found2);//dsdfwfwdfr
      }
      else{
        System.out.println("result cannot be found");
      }
     
      
    }//b-d binary search
      
    
    
    
   
    
  }
  
 
  
  
  
  
  
  
  
  
  
  
  
  
}
