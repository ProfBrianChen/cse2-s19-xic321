//Hailey Cai CSE2 Hw06 Network
//Mar 18th, 2019

// Scenario 1:  square size is even,
//  edges should be double width 
//  ______
//  ------
// Scenario 2: square size is odd
// use only single "-" characters
//  ------
// Square corner  uses "#" 


//           input your desired height: 
//           input your desired width:
//           input square size : 
//           input edge length 
//           ALL SHOULD BE INTEGER, if not, ask for input again


// step1 : evaluate positive integer using myScanner 
// step2 : 4 substeps to build up 



import java.util.Scanner;     //import Scanner class
public class Network{
  public static void main(String[] args){
    

  
     Scanner myScanner = new Scanner(System.in);
    
    
    //ask the user for an input  
    //use while loop to test Integers, if it is not, clear the memory and type one more 
    //do the same for all input variables 
    

        // height
        int height;
        System.out.print("Input your desired height: ");
        while (true) {
          while (!myScanner.hasNextInt()) {
            System.out.println("wrong type for height. try again: ");
            myScanner.next();
          }
          height = myScanner.nextInt();
          if (height <= 0) {
            System.out.println("height value out of range. try again: ");
          } else {
            break;
          }
        }

        // width
        System.out.print("Input your desired width: ");
        int width;
        while (true) {
          while (!myScanner.hasNextInt()) {
            System.out.println("wrong type for width. try again: ");
            myScanner.next();
          }
          width = myScanner.nextInt();
          if (width <= 0) {
            System.out.println("width value out of range. try again: ");
          } else {
            break;
          }
        }

        // square size
        int squareSize;
        System.out.print("Input square size: ");
        while (true) {
          while (!myScanner.hasNextInt()) {
            System.out.println("wrong type for squareSize. try again: ");
            myScanner.next();
          }
          squareSize = myScanner.nextInt();
          if (squareSize < 0) {
            System.out.println("squareSize value out of range. try again: ");
          } else {
            break;
          }
        }
        // edge length
        int edge;
        System.out.print("Input edge length: ");
        while (true) {
          while (!myScanner.hasNextInt()) {
            System.out.println("wrong type for edge. try again: ");
            myScanner.next();
          }
          edge = myScanner.nextInt();
          if (edge < 0) {
            System.out.println("edge value out of range. try again: ");
          } else {
            break;
          }
        }
//end of userinput 
    
    
    
       
        int lineFlag = squareSize % 2;                                      // if squaresize is odd, we need one edge line,
                                                                            // if squaresize is even, we need two edge line
        for (int i = 0; i < height; i++) {
            int mod = i % (squareSize + edge);                              // use mod (remainder method) to check each row whether it falls under the square area or edge area
            for (int j = 0; j < width; j++) {                               // if mod is smaller than the squaresize, it lies in the square, otherwise, the edge
                int columnMod = j % (squareSize + edge);                    // use columnMod to check each column, 
                                                                            // if columnMod is greater than squareSize, it is in the square area, otherwise it is in the edge area
                if (mod < squareSize) {                                     // scenario 1: when mod is smaller than squareSize: 
                                                                            // (1) columnMod is greater than sqSize : draw the vertical edgeLine "-"
                                                                            // (2) columnMod is smaller than sqSize : draw the square and change "#" for corner                                                        
                    //fall under inside the square area
                    if (columnMod < squareSize) {//(1) ========== ALL ABOUT THE SQUARE
                        //columnMod column output square
                        if (columnMod == 0 || columnMod == (squareSize - 1)) {
                            //target the first and last character in a square in a row 
                            if (mod == 0 || mod == (squareSize - 1)) {
                                //target the first and last character in a square in a column
                                System.out.print("#");
                            } else {
                                System.out.print("|");      // print | for the rest of in a column
                            }
                        } else {
                            //if (in the first and last rwo), not the corner
                            if (mod == 0 || mod == (squareSize - 1)) {
                                //not the corner, but the first and last row 
                                System.out.print("-");
                            } else {// inside print " "
                                System.out.print(" ");
                            }
                        }
                    } else { 
                        //edgeLine
                        if (lineFlag == 0) {      
                            //when squareSize is even
                            if (mod == squareSize / 2 || mod == squareSize / 2 - 1) { //the middle two rows to be printed edge line "-"
                                System.out.print("-");
                            } else {   //  other rows can be " "
                                System.out.print(" ");
                            }
                        } else {
                            //odd squaresize
                            if (mod == squareSize / 2) {       //
                                System.out.print("-");
                            } else {
                                System.out.print(" ");
                            }
                        }
                    }
                    //endregion (1/2) when mod<squaresize 
                } else {                                                                      //scenario 3/4: when mod > squareSize:::
                    // region (3) print "|"                                                   //(3) columnMod < squareSize :print | for the i==0 and i==squaresoze-1
                                                                                                                          //print the rest " "
                                                                                              //(4) columnMod > squareSize :print | for the ... same as above
                    if (columnMod < squareSize) {
                        if (lineFlag == 0) {
                            //偶数行的话
                            if (columnMod == squareSize / 2 || columnMod == squareSize / 2 - 1) {
                                System.out.print("|");
                            } else {
                                System.out.print(" ");
                            }
                        } else {
                            //when squaresize is odd, middle column print edge line 
                            if (columnMod == squareSize / 2) {
                                System.out.print("|");
                            } else {
                                System.out.print(" ");
                            }
                        }
                    } else {
                        System.out.print(" ");
                    }
                        //endregion (2):
                }
                //endregion
            }
            //switch a new line
            System.out.println();
        }

  
  
  
}
}