//Hailey Cai, CSE2
//buildCity():randomly generate rectangle (10-15)*(10*15)
//            each block is random from(100-999)
//display():  it shows blocks use printf to format 
//invade():   random k robots to change the blocks to negative 
//update():   update 5 times, to move the negative sign to the right everytime you update 
//            check the first column should always to positive 
//            last column, negative sign goes away when it hits the edge (do not use *(-1)
//Notes*      when two negative signs are together,both move to the right 
              //check the city[i][j-1], see if the previous number is a negative or not.check
              // if previous one is negative, change[i][j] to negative (regardless of its current status)
              // if previous one is positive, make [i][j] abs()
              


public class RobotCity {
    public static void main(String[] args){
        System.out.println("What a peaceful city:)");
        int a[][] =buildCity();
        display(a);  //show 1
        System.out.println();
        int k=(int)(Math.random()*15+3); //random a integer as number of robots
        System.out.println("Oh no, We are getting attack!!");
        int b[][] =(invade(a,k));
        display(b); //show 2
        System.out.println();
        System.out.println("Update!"); //loop 5 times
        int newarr[][] =update(b);
        display(newarr); //update 1
        System.out.println();

        for(int i=0;i<4;i++){
            newarr=update(newarr);
            display(newarr);
            System.out.println();
        }
    }//end main
    public static int[][] buildCity(){    //generate the grid/rectangle
        int rowNum=(int)(Math.random()*6+10);
        int colNum=(int)(Math.random()*6+10);
        int [][] city=new int[rowNum][colNum];
        for(int i=0; i<city.length;i++){
            for(int j=0;j<city[0].length;j++){
                int pop= (int)(Math.random()*900+100);
                city[i][j]=pop;
            }
        }
        return city;
    }//end buildCity
    public static void display(int[][] city){
       // for (int j = 0; j<city[0].length; j++)
        for(int i=0;i<city.length;i++){
            for (int j = 0; j<city[0].length; j++) {
                System.out.printf(" %d\t ",city[i][j]); //format it as spreadsheet grid
            }
            System.out.println();
        }
    }//end display
    public static int[][] invade(int[][] city, int k){  //k is the number of robot
        String[] xy=new String[k];
        int count=0;
        while(true) { 
          // k numbers of robots and make sure there are not landing on the same block 
            int x = (int) (Math.random() * city.length); //randomly generate x coordinate
            int y = (int) (Math.random() * city[0].length); //randomly generate y coordinate
            String tempKey = x + "-" + y;
            boolean flag = false;
            for (int i = 0; i < xy.length; i++) {
                if (tempKey.equals(xy[i])) {   //count # of robots no repeat
                    flag = true;
                }
            }
            if(flag){
                continue; //repeat
            }else{
                xy[count] = tempKey;
                if(count==k-1){
                    break;
                }
                count++;
            }
        }
        for (int i = 0; i < xy.length; i++) {
            String tempKey =xy[i];
            String[] split = tempKey.split("-");
            int x=Integer.valueOf(split[0]); //split x,y coordinate back to integer
            int y=Integer.valueOf(split[1]);
            city[x][y]=city[x][y]*(-1); //make the block negative (where the robots hit)
        }
        return city;
    }//end invade
    public static int[][] update(int[][] city){
        int[][]newarr =new int[city.length][city[0].length];
        for(int i=0;i<city.length;i++){
            for (int j = 0;j<city[0].length; j++) { //just in case if two negative numbers appear together
                if(j>0&&city[i][j-1]<0){
                    //if not the 1st column , if the position before[i][j] is negative, [i][j] has to be negative
                    newarr[i][j] = city[i][j]<0?city[i][j]:city[i][j]*-1;    //return negative !!
                }else {//if the previous one is positive 
                    newarr[i][j] = city[i][j]>0?city[i][j]:city[i][j]*-1;    //if [i][j] is positive ---abs()
                                                                          
                }
            }
        }
        return newarr;
    }//end update
}









