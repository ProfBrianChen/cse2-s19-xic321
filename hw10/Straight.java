//Hailey Cai, CSE2 
//05/02/2019
//shuffled() get a deck of 52 cards
//draw() 5 cards as array sort it(selection sort),
//searchK() find the kth lowest value
//findstraight() convert deck into 0-12 using mod, boolean
//draw 1000000 times and get the probablity of straight 
//*note call searchK() to compare value, the sorted array from first position and the second to the last,
//if they equals add up to 4, then count 1 striaght
//find the total straight by looping findstraight 
//get the total count and do the math..

public class Straight {
    public static void main(String[] args){
        
        int k=(int)(Math.random()*5+1);   //search kth lowest from 1-5
//         int kVal= searchK(fivecards,k);
//         System.out.print("the kth lowest value is "+kVal);
      //  int[] mod=findStraight(fivecards);
        System.out.println();
        int count=0;
        boolean flag=true; // yes there is straight 
        for(int i=0;i<1000000;i++){
             int[] a=shuffled();
             int[] fivecards= draw(a);
            if(findStraight(fivecards)==flag){
              count++;
            }else{
              continue;
            }
        }
       
      double percent =(count/1000000.0)*100;
      System.out.println("Out of 1000000 draws, there are total of "+count+" striaght,");
      System.out.println("the probability is "+ percent+"%");  
    }    

    public static int[] shuffled(){     //shuffle a deck of cards 0-51
        int count=0;
        int arr[]=new int[52];
        while(true){
            boolean flag=false; 
            int temp=(int)(Math.random()*52);
            for(int i=0;i<arr.length;i++){
                if(arr[i]==temp){ //repeat
                    flag=true;
                }
            }
            if(flag){ //repeat
                continue;
            }else{
                arr[count]=temp;
                if(count==50){
                    break;
                }else{
                    count++;      //if not repeat, add to count until 51
                }
            }
        }
        return arr; //randomly allocated 51 cards
    }
    public static int[] draw(int[] arr){ //draw 5 cards in the deck of 52 cards  
        int [] fivecards=new int[5];
        for(int i=0;i<5;i++){
            fivecards[i]=arr[i];
        }
        return fivecards;
    }
    public static int searchK(int[] arr, int k){ //input arr is unsorted //search the kth lowest number in the array
      int temp=0;
   //   int newarr[]=new int[5];
        if(k<0||k>5){
            return -1;
        }else{
            for(int i=0;i<arr.length-1;i++){
                int min=i;
                for(int j=i+1;j<arr.length;j++){
                    if(arr[j]<arr[min]){
                        min=j;
                    }
                }
              temp=arr[min];
              arr[min]=arr[i];
              arr[i]=temp; 
            }//sort array
              int kVal = arr[k-1];
              return kVal;
        }
    }//end searchK
    public static boolean findStraight(int[] arr){ 
      int mod;
      int kVal;
      int count=0;
      int first, second;
        for(int i=0;i<arr.length;i++){
            mod=arr[i]%13;
            arr[i]=mod; //convert to 0-12
        }
        for(int k=1;k<=5;k++){
            first=searchK(arr,k);
            second=searchK(arr,k+1);
            if(first+1==second){
                    count++;
            }
        }
        if(count==4){
          return true;
        }else{
          return false;
        }
    }

}



















