//lab04, Hailey Cai, cse02
// pick a random card, use random number generator (from 1-52)
// cards 1-13 diamonds
// cards 14-26 clubs
// cards 15-28 hearts
// cards 29-52 spades
// 2 string variables : name + identity 
// if statement to assign the suit name
// switch statement to assign the card identity 
// print out the name of randomly selected card 



import java.util.Random;
public class CardGenerator{
  public static void main(String[] arg){
    
    int num = (int)(Math.random()*52)+1;   //generate a random number btw 1-52
    String suit = null;   
    String cardIdentity = null;             // declare 2 string type variable to identity the suit and num
    
    if(num <=13){
      suit ="Diamonds";
    }
    else if(num>=14 && num <= 26){
        suit = "Clubs";
    }
    else if(num>=26 && num <=39){
       suit ="Hearts";
    }
    else if(num >= 40 && num <= 52){
       suit ="Spades";
    }                                       // if statement to identity suit types
                 
                    
    int remain = num%13;
    switch(remain){
      case 0: 
      cardIdentity ="King";
      break;
      case 1:
      cardIdentity = "Ace";
      case 11:
      cardIdentity = "Jack";
      case 12:
      cardIdentity = "Queen";
    }                                     // switch statement to convet 1, 11,12,13 to ace, jack, queen and king
   
  System.out.print("You picked the " + cardIdentity + " of " + suit);
        
    
  }
  
  
}