//CSE 02, Hailey Cai, 02/05/19
// calculate following:
// total cost of each kind of item
// sales tax charged buying all of each kind of item
// total cost of purchases (before tax)
// total sales tax
// total paid for this transaction, including sales tax


public class Arithmetic {
    public static void main(String args[]){
      int numPants =3; //number of pairs of pants 
      double pantsPrice=34.98; //cost per pair of pants     
      int numShirts= 2; // number of sweat shirts
      double shirtPrice =24.99; // cost per sweat shirt
      int numBelts =1; // number of belts
      double beltCost =33.99; // cost per belt
      double paSalesTax =0.06; // tax rate
      
      
      double totalCostOfPants, totalCostOfShirts, totalCostOfBelts, totalSalesTax, 
      totalCostBeforeTax, totalCostAfterTax;  // define all variables
      
      totalCostOfPants= (double)numPants*pantsPrice; 
      totalCostOfShirts = (double)numShirts * shirtPrice; 
      totalCostOfBelts = (double)numBelts * beltCost; //total cost of each item
           
    
      double taxPants = totalCostOfPants* paSalesTax; 
      double taxShirts = totalCostOfShirts* paSalesTax;
      double taxBelts = totalCostOfBelts * paSalesTax;
      //sales tax for each item 
      

      double a = Math.round(taxPants*100.0)/100.0;
      double b = Math.round(taxShirts*100.0)/100.0;
      double c = Math.round(taxBelts*100.0)/100.0; // round to 2 decimals
      
        
      totalCostBeforeTax = totalCostOfPants + totalCostOfBelts + totalCostOfShirts;
      // total cost of purchases (before tax)
           
      totalSalesTax = taxPants + taxShirts + taxBelts;
      // total sales tax
      
      totalCostAfterTax =  totalSalesTax+totalCostBeforeTax;
      // total cost after tax
      
      double roundTotalSalesTax= Math.round(totalSalesTax*100.0)/100.0;
      double roundTotalCostBeforeTax = Math.round(totalCostBeforeTax*100.0)/100.0;
      double roundTotalCostAfterTax = Math.round(totalCostAfterTax*100.0)/100.0;
      //round to 2 decimals
      
      
      System.out.println("the cost of pants is " +totalCostOfPants);
      System.out.println("the cost of shirts is " +totalCostOfShirts);
      System.out.println("the cost of belts is " +totalCostOfBelts);
      System.out.println("Sales Tax on pants is " + a);
      System.out.println("Sales Tax on shirts is " + b);
      System.out.println("Sales Tax on belts is "+ c);
      System.out.println("Total sales tax is " + roundTotalSalesTax);
      System.out.println("Total cost of purchase before tax is " 
                       + roundTotalCostBeforeTax);
      System.out.println("Total cost of purchase after tax is " + 
                       roundTotalCostAfterTax);
      //display all values
      
    }
}
