//CSE2 Hailey Cai
//03/24/2019
//Input rectangle/circle/triangle
// user input "triangle'circle'rect: rectangle
// input user height: 


// main method 
// method 1: triangle 
// method 2: rectangle
// method 3: circle
// method 4: input method: with scanner 
// ask user to print height width .. base on the shape they choose

import java.util.*;
public class Area{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    System.out.print("input a triangle, rectangle or a circle: "); //ask user to input triangle/rectangle or circle
    String inputShape = myScanner.nextLine();
    
    
    while(true){
      while(!inputShape.equals("triangle")&&!inputShape.equals("rectangle")&&!inputShape.equals("circle")){
           System.out.print("Error! type another shape: ");
           inputShape=myScanner.next();
      }
      if(inputShape.equals("triangle")){// if the user type in triangle, call the triangle method
        System.out.println("input a height: "); // ask the user to input height and base
        double height = check();//call the check method to check the height if it is a double type
        System.out.println("input a base: ");
        double base= check();//call the check method to check base is double 
        double aTriangle= triangle(height,base); //invoke method with two input values 
        System.out.print("the area of triangle is: "+aTriangle); // calculate the area 
        break;
        
      }
      else if(inputShape.equals("rectangle")){ //call rectangle method
    	  System.out.println("input a height ");//ask user to input height and width 
    	  double heightRectangle = check();//check height 
    	  System.out.println("input a width ");
    	  double widthRectangle =check();
    	  double aRectangle =rect(widthRectangle,heightRectangle);
          System.out.print("the area of rectangle is : "+aRectangle);
          break;
      }
      else if(inputShape.equals("circle")){// ask user to input radius after calling circle method
          System.out.println("input a radius: ");
          double radius =check();
          double aCircle= circle(radius);
          System.out.print("the area of circle is : "+aCircle);
    	  break;
      }    
     }  //end while loop
    
    
   }
  
  

  
   public static double rect(double width, double height){//method to calculate rectangle
   double a = width*height; //formulas to get the rectangle area 
   return a;

   }

   public static double triangle(double height, double base){//triangle 
     double a= height *base*0.5;// formulas to get triangle area 
     return a;
   }
  
   public static double circle(double radius){// circle 
     double a= Math.PI*radius*radius;//formula to check circle area 
     return a;
   }
  
  
  public static double check(){// check input value 
   Scanner sc = new Scanner(System.in);
  
		        while(!sc.hasNextDouble()){
		             System.out.print("Error, please input another DOUBLE value: ");
		             sc.next();
		          }
		       double val = sc.nextDouble();//initialize userinput and make it   
				return val;
   
  }
  
  
  
  
}
