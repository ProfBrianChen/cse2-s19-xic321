//CSE2 Hailey Cai 
//03/28/2019
//two method same name 
// method 1: accept string 
// method 2: accept string and int
//examine string with or without numbers

import java.util.*;
public class StringAnalysis {
	public static void main(String[] args) {

		      Scanner myScanner = new Scanner(System.in);
		      System.out.print("input a String: ");
		      String inputString = myScanner.nextLine(); // String with multiple characters
		      System.out.println("Do you want to examine the entire String. If yes, type 'yes'. If no, type in an integer");
		     //ask user to type in an string message
		      
		      
		    while(true){
		    	if(myScanner.hasNextInt()) { //check if the user input is a valid message (either type in a number or type in 'yes')
		    		int message = myScanner.nextInt(); 
		    		method(message,inputString);
		    		break;
		    		
		    	}else {
		    		String message = myScanner.nextLine();
		    		if(message.equals("yes")) {//when the user type in yes- which means to check the entire string message
		    			 method(inputString);
		    		}
		    		else {//when the user type in nether a 'yes' nor a number
		    			System.out.print("Error, do you want to examine the whole thing? Please type in 'yes' or a number!!! ");
		    		}
		    	}
		    	

		    }//endWhileLoop
		   
	}//end main
		  public static boolean method(int num, String s ){//int +string
		      char position='0';
		      boolean flag=false;
			     
		      for(int i=0;i<num;i++){
		        position = s.charAt(i); //place each position using i 
		       
		         if(!Character.isLetter(position)){//check each poisition whether it is letter 
		           System.out.println("The String characters are NOT made of all letters. "); //if it is not a letter, print
		           flag = true;
		           System.exit(0);   //exit the system 
		           
		     }    
		      }
		  	if(flag==false) {//run the whole thing but no number
		   		System.out.println("The String is made of all letters. No numbers!");
		   		System.exit(0);//exit the system
		   		
		   	}
		    return true;
		  }//end method string/int 
		  
		  public static boolean method(String s){//string
		    char position ='0';
		    boolean flag=false;
		     
		   for(int i=0;i<s.length();i++){ //check the string message from the first position 
		       position= s.charAt(i);
		       
		     if(!Character.isLetter(position)){  // when the character is not a letter
		       System.out.println("The String characters are NOT made of all letters.");  
		       flag =true;
		       System.exit(0);// exit the system 
		     }
		   
		      }//end for loop
		   	if(flag==false) {//run the whole thing but no numbers
		   		System.out.println("All characters are letters!");
		   		System.exit(0);//exit the system 
		   		
		   	}
		    return true;
		  }//method string
		  
		  
		
	

}
