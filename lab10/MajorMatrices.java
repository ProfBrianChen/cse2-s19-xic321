//Hailey Cai, cse2
//lab10 generate 2D arrays 





public class MajorMatrices {
    public static void main(String[] args){
       int width =(int)(Math.random()*8);
       int height=(int)(Math.random()*8);
    	
       int[][] arrRow= increasingMatrix(height,width,true);
       System.out.println("Generating row-major matrix with width"+ width+" and height " +height);
       printMatrix(arrRow, true);
       int[][]arrCol=increasingMatrix(height,width,false);
       System.out.println();
       System.out.print("Generating column-major matrix with width"+ width+" and height " +height);
       System.out.println();
       printMatrix(arrCol, false);

       int wid=(int)(Math.random()*10);
       int hei=(int)(Math.random()*10);
       int[][]array3 =increasingMatrix(wid,hei,true);
       System.out.print("Generating a matrix with width"+wid+" and height "+hei);
       System.out.println();       
       printMatrix(array3, true);
       System.out.print("Adding two matrices");
       System.out.println();
       int[][]add =addMatrix(arrRow,true,arrCol,false);
       for(int i=0;i<add.length;i++) {
    	   for(int j=0;j<add[0].length;j++) {
    		   System.out.print(add[i][j]+" ");
    	   }
    	   System.out.println();
       }
       if(array3.length!=add.length) {
    	   System.out.print("Unable to add input matrices");
       }else {
    	   int[][]add2 =addMatrix(add,true,array3,true);
           for(int i=0;i<add2.length;i++) {
        	   for(int j=0;j<add2[0].length;j++) {
        		 System.out.print(add2[i][j]);
        	   }
        	   System.out.println();
           }
       }
      
    }//end main
    
    public static int[][] increasingMatrix(int height,int width, boolean format){
    	int i=0,j=0;
    	int count =1;
    	if(format) { //row major
    		int[][] arr=new int[height][width];
    		for(i=0;i<arr.length;i++) {
        		for(j=0;j<arr[0].length;j++) {
        			arr[i][j]=count;
        			//System.out.print(count+" ");
        			count++;
        		}
        	}
    		return arr;
    	}else { //column major 
    		int[][] arr=new int[width][height];
    		for(j=0;j<arr[0].length;j++) {
    			for(i=0;i<arr.length;i++) {
    				arr[i][j]=count;
    				//System.out.print(arr[i][j]+" ");
    				count++;
    			}	
    		}
    		return arr;
    	}
    }
    
    public static void printMatrix(int[][] array, boolean format ){
    	 if(format) {
    		 if(array.length==0||array[0].length==0) {
    			 System.out.print("the array is empty");
    			 System.exit(0);
    		 }else { //row major with value
    			 for(int i=0;i<array.length;i++) {
    		      	   for(int j=0;j<array[0].length;j++) {
    		      		   System.out.print(array[i][j]+" ");
    		      	   }
    		      	 System.out.println();
    		     }
    		 }
    	 }else {
    		 for(int j=0;j<array[0].length;j++) {
    			 for(int i=0;i<array.length;i++) {
    				 System.out.print(array[i][j]+" ");
    			 }
    			 System.out.println();
    		 }
    	 }//end if else 
    	
    }//end print
    
    public static int[][] translate(int[][] array){ //assume its col-major 
    	int [][] translateArr=new int[array[0].length][array.length];
    	for(int i=0;i<translateArr.length;i++) {
    		for(int j=0;j<translateArr[0].length;j++) {
    			translateArr[i][j]=array[j][i];
    		}
    	}
    	return translateArr;
    }
    
    public static int[][] addMatrix( int[][] a, boolean formata, int[][] b, boolean formatb){
    		if(formata ==false) { //if a is column major
    			a=translate(a); 
    		}
    		if(formatb==false) {
    			b=translate(b);
    		}
    		int aheight= a.length;
        	int awidth = a[0].length;
        	int bheight= b.length;
        	int bwidth = b[0].length;
    		if(aheight !=bheight || awidth!=bwidth) {
        		System.out.print("the arrays cannot be added!");
        		return null;
        	}
    		else {
    		int[][] addarr=new int [a.length][a[0].length];
    		for(int i=0;i<addarr.length;i++) {
    			for(int j=0;j<addarr[0].length;j++) {
    				addarr[i][j]=a[i][j]+b[i][j];
    			}
    		}
    		return addarr;
    		}
    }
}
