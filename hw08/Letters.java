//Hailey Cai, CSE2
//April 7th, 2019
// print a string, examine in order from A-M, N-Z(with capitalized and non-capitalized)
// two methods, one for A-M, another for N-Z
// input the string in the main method 




import java.util.Scanner;
public class Letters{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); 
    System.out.print("Random character array: "); // ask user for an string
    char[] arr=myScanner.next().toCharArray();
  
    String result1 = getAtoM(arr);
    System.out.println("AtoM characters: "+ result1); //call method getAtoM
    String result2 = getNtoZ(arr);
    System.out.println("NtoZ characters: "+ result2); //call method getNtoZ
    
  }//end main
  
  
  public static String getAtoM(char[] arr){
    String result1 = "";
    for(int i=0;i<arr.length;i++){
      if((arr[i]<='m'&& arr[i]>='a')||(arr[i]<='M'&& arr[i]>='A')){
        result1+=arr[i];
      }
      //if check arr[i] is between 'a' - 'm'  'A' - 'M'
      //    result += arr[i] 
    }
    return result1; //return the String after sorted
  }//end getA-M
  
         
         
         
  public static String getNtoZ(char[] arr){
    String result2="";
    for(int i=0;i<arr.length;i++){ //for loop to check each position
      if((arr[i]<='Z'&&arr[i]>='N')||(arr[i]<='z'&&arr[i]>='n')){
        result2+=arr[i];    // if the character is within the range, store in the result2 
      }
    }
    return result2; //return the string after sorted
  }//end getN-Z
}