//Hailey Cai
//CSE2 April 7th 
//hw08 Program spec: five random numbers from 0-59
//use two methods : one to generate the lottery number, another to compare the userinput and the lottery



import java.util.*;
public class PlayLottery{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter 5 numbers between 0 and 59: ");//user input values btw 0-59
    int [] user=new int[5];
    for(int i=0;i<user.length;i++) {
    	 user[i] = myScanner.nextInt();
    }
    boolean result =userWins(user, numbersPicked());
    if (result == false) {
    	System.out.println("You lost");
    }
  }//end main
  
  public static boolean userWins(int[] user, int[] winning){ //compare values 
	int sameNum=0; // count the number of same values (one from userinput, another from lottery)
	System.out.print("The winning numbers are: ");
	for(int i =0; i < winning.length; i++) { 
		if (i != winning.length - 1) { //the last input without comma ","
			System.out.print(winning[i] + ", ");
		}
		else {
			System.out.print(winning[i]);  //the first to the fourth number with a comma 
		}
	}
	System.out.println();
   
    for(int i=0;i<5;i++) {
    	for(int j=0;j<5;j++) {
    		if(user[i]==winning[i]) { //compare the value from user to winning person. 
    			sameNum++;
    			if(sameNum==5) { //if there are 5 same numbers, you win the lottery
    				return true;
    			}
    		}
    	}
    }
	  return false; //when the user do not win the lottery(when there are no five same numbers)
  }//end userWins

  
  public static int[] numbersPicked(){//random number the lottery picked 
	  int[] winning= new int[5];
	  for(int i=0;i<winning.length;i++) {
		  winning[i]=(int)(Math.random()*60); 
	  }
	  
	  return winning;
  }//end numbersPicked

  
  
  
  
  
  
  
  
  
}