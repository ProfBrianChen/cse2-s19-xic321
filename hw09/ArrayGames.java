//Hailey Cai, CSE2, hw09
//April 16th, 2019
//
//method1: method generate() , random size btw 10-20
// .       produce array
//method2: method print() 
//         print out the members of an integer array input.
//method3: method insert() 
//          input: two arrays
//          produce a new array long enough to contain two arrays
//         input array and an input integer. 




import java.util.*;
public class ArrayGames{
  public static void main(String[] args){
    Scanner myScanner= new Scanner(System.in);
    System.out.print("Do you want to insert or shorten an array? Type in 'insert' or 'shorten': ");
    String answer = myScanner.next(); //ask input for either insert or shorten the array
    int[] arr= generate();
    if(answer.equals("insert")){
        int[] arr1 = generate();
        print(arr1,"input1: ");
        int[] arr2 = generate();
        print(arr2,"input2: ");
        int[] arrInsert= insert(arr1, arr2);
        print(arrInsert,"output: ");
    }
    if(answer.equals("shorten")){
    	  int randomInt = (int)(Math.random()*30);
        print(arr, "input1: ");
        System.out.println("input2: "+randomInt);
        int[] shortArray = shorten(arr, randomInt);
        print(shortArray,"output: ");
    }
    
  }//end main
  
  public static int[] generate(){    //produce an array with random size btw 10-20 
    int arrSize= (int)(Math.random()*11+10);
    int arr[] = new int[arrSize];
    arr[0]=(int)(Math.random()*10+1);
    for(int i=1;i<arr.length;i++){   //i start with a random number 
      arr[i]=arr[i-1]+1;
    }

    return arr;
  }//end generate method
  public static void print(int[] arr,String name){ //print out array
	  
	  System.out.print(name);
	  for(int i =0;i<arr.length;i++) { 
		  System.out.print(arr[i]+" "); 
	  }
	  System.out.println();
	  
  }//end print method
  
  public static int[] insert(int[] arr1, int[] arr2){ //insert second array into the first array
	int arr1Length= arr1.length;
    int arr2Length = arr2.length;
    int index = (int)(Math.random()*arr1Length-1);
    int len = arr1.length+arr2.length; //the length of new array
    int[] newarr= new int[len];
    for(int i=0;i<len;i++) { //the new array
    	if(i<index) { //when the position i is small than index(the insertion point), 
    		newarr[i]=arr1[i];  //print the i (in the first array)
    	}
    	else{  //when i >= index, print second array and the rest of the first array
    		if(i-index<arr2.length) { // print second array
    			newarr[i]=arr2[i-index];
    		}
    		else { //print the rest of the first array
    			newarr[i]=arr1[i-arr2.length];
    		}
    		
    	}
    }
    	
    return newarr; //return new array
  }//end insert
  
  
  
  
  public static int[] shorten(int[] arr, int index){
    int arrlength = arr.length;
    int [] arrnew= new int[arr.length-1]; //create a new array to store the 
    
      if(arrlength>index){ // if index is inbound
          for(int i=0;i<arrnew.length;i++){
              if(i<index){
            	  arrnew[i]=arr[i];
              }
              else {  
            	  arrnew[i]=arr[i+1];
              }
          }
       
          return arrnew;
      }else{//if index is out of bound, return the original array
          return arr;
      }
    
    
    
  
  }//end shorten
  
  
}