//lab08 Arrays, Hailey Cai, CSE2
// 

// 1: getStdDev
// 2: getMean
// 3: getRange -Arrays.sort 

import java.util.*;
public class Arrays{
  public static void main(String[] args){
    
    int entrySize = (int)((Math.random()*51)+50);   //get a random num from 50-100
    int arr[] = new int[entrySize]; //declare array and assign entry size to the array
    //array size is "entrySize"
   
    System.out.println("the array size is: "+entrySize);  
     //print the size of array
    
    for(int i =0;i<arr.length;i++){
      arr[i] = ((int)(Math.random()*100));  //each entry is populated with random num from 0-99
      //System.out.println("the content is " + arr[i]);  
    }
     
    double mean =getMean(arr);
    System.out.println("the mean is "+mean);
    System.out.println("the range is "+getRange(arr));
    System.out.println("the standard deviation is "+getStdDev(arr,mean));
    
    
    System.out.println("*****");
    System.out.println("*****");

    shuffle(arr);
   
  }//end main
  
  
  public static double getRange(int[] arr){
  //  Arrays.sort(arr);
    int range= arr[arr.length-1]-arr[0];   //max-min
    int min=arr[0];
    int max=arr[0];
    for(int i=0;i<arr.length;i++){
    if(min>arr[i]){
      min=arr[i];
    }
    if(max<arr[i]){
      max=arr[i];
    }
    }
    range=max-min;
    return range;
    
    
  }//end range
  
  public static double getMean(int[] arr){
    int sum=0;
    double mean=0;
    for(int i=0;i<arr.length;i++){
      sum=sum+arr[i]; 
    }
    mean =sum/arr.length;
    
    return mean;
  }//end mean
  
  
  public static double getStdDev(int[] arr, double mean){
    double temp=0;
    double std=0;
    for(int i=0;i<arr.length;i++){
      temp=(arr[i]-mean)*(arr[i]-mean)+temp;
    }
    std=temp/(arr.length-1);
    double s= Math.sqrt(std);
    
    
    return s;
  }//end std
  
  public static void shuffle(int[] arr){
      int size = arr.length;
	  for(int i=0;i<arr.length-1;i++){
       int swap = (int)((Math.random()* size));
       arr[i]=arr[swap];
       System.out.println("arr["+i+"] = " + arr[i]);
     }
    
    
    
    
    //print out shuffled array entry
    
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}