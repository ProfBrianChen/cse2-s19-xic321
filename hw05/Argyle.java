//Hailey Cai, Extra credit hw5
//CSE2 - March 19th, 2019
// argyle stripe width must be odd and not larger than half of the diamond size
// 
import java.util.Scanner;
public class Argyle{
  public static void main(String[] args){
  Scanner myScanner = new Scanner(System.in);
  //ask the user for input  
  //use while loop to test the positive integers, if it is not, clear the memory and type one more 
  //do the same for all input variables 
  // =================================================== 
  // width of Viewing window
  int width;
  System.out.println("please enter a positive integer for the width of Viewing window in characters: ");
  while (true) {
    while (!myScanner.hasNextInt()) {
      System.out.println("wrong type for width. try again: ");
      myScanner.next();
    }
    width = myScanner.nextInt();
    if (width <= 0) {
      System.out.println("width value out of range. try again: ");
    } else {
      break;
    }
  }
  // =================================================== 
  // height of Viewing window
  int height;
  System.out.println("please enter a positive integer for the height of Viewing window in characters:");
  while (true) {
    while (!myScanner.hasNextInt()) {
      System.out.println("wrong type for height. try again: ");
      myScanner.next();
    }
    height = myScanner.nextInt();
    if (height <= 0) {
      System.out.println("height value out of range. try again: ");
    } else {
      break;
    }
  }

  // height of Viewing window
  // =================================================== 
  // width of the argyle diamonds 
  int diamondWidth;
  System.out.println("please enter a positive integer for the width of the argyle diamonds: ");
  while (true) {
    while (!myScanner.hasNextInt()) {
      System.out.println("wrong type for diamondWidth. try again: ");
      myScanner.next();
    }
    diamondWidth = myScanner.nextInt();
    if (diamondWidth < 0) {
      System.out.println("diamondWidth value out of range. try again: ");
    } else {
      break;
    }
  }

  // =================================================== 
  //positive odd integer fror the width of the argyle center stripe
  int centerStripe;
  System.out.println("please enter a positive odd integer for the width of the argyle center stripe: ");
  while (true) {
    while (!myScanner.hasNextInt()) {
      System.out.println("wrong type for centerStripe. try again: ");
      myScanner.next();
    }
    centerStripe = myScanner.nextInt();
    if (centerStripe % 2==0||centerStripe<0) {
      System.out.println("centerStripe value out of range. try again: ");
    } else {
      break;
    }
  }

           System.out.print("please enter a first character for the pattern fill: ");
           String temp1 = myScanner.next();
           char f = temp1.charAt(0);  //first character(char) for the pattern 

           System.out.print("please enter a second character for the pattern fill: ");
           String temp2 = myScanner.next();
           char s = temp2.charAt(0);  //second character(char) for the pattern
    
           System.out.print("please enter a third character for the pattern fill: ");
           String temp3 = myScanner.next();
           char t = temp3.charAt(0);  //third character(char) for the pattern


                for (int i = 0; i < height; i++) {
                    int rowMod = i % (diamondWidth * 2);
                    int reRowMod = diamondWidth * 2 - rowMod;
                    for (int j = 0; j < width; j++) {
                        int columnMod = j % (diamondWidth * 2);
                        int reColumnMod = diamondWidth * 2 - columnMod - 1;

                        boolean stripeLineFlag = false;
                        boolean sCharFlag = false;
                        //region 
                        int stripeLineParam =0;
                        stripeLineParam= (centerStripe + 1) / 2;
                        if (Math.abs(rowMod - columnMod) < stripeLineParam) {
                            stripeLineFlag = true;
                        }
                        if (Math.abs(rowMod - reColumnMod) < stripeLineParam) {
                            stripeLineFlag = true;
                        }

                        //endregion

                        //region output char s
                        //find the middle (symmetric line )
                        int rowMiddle, columnMiddle;
                        columnMiddle = diamondWidth;
                        if (diamondWidth % 2 == 0) {
                            rowMiddle = diamondWidth + 1;
                        } else {
                            rowMiddle = diamondWidth;
                        }
                        if (rowMod < rowMiddle) {
                            if (columnMod < columnMiddle) {
                                if (columnMod >= rowMiddle - rowMod - 1) {
                                    sCharFlag = true;
                                }
                            } else {
                                if (reColumnMod >= rowMiddle - rowMod - 1) {
                                    sCharFlag = true;
                                }
                            }

                        } else {
                            if (columnMod < columnMiddle) {
                                if (columnMod > rowMod - rowMiddle) {
                                    sCharFlag = true;
                                }
                            } else {
                                if (reColumnMod > rowMod - rowMiddle) {
                                    sCharFlag = true;
                                }
                            }
                        }

                        //endregion

                        if (stripeLineFlag) {
                            System.out.print(t);    
                        } else {
                            if (sCharFlag) {
                                System.out.print(s);
                            } else {
                                System.out.print(f);  
                            }
                        }
                    }
                    System.out.println();
                }
    
    
    
    
    
    
    
    
    
    
    
    
  }
  
}