//Hailey Cai, CSE02, Hw05
//Mar 2nd, 2019
//program spec:
//write a loop to ask user type in infor:
//course number(int), department name(String), number of times it meets per week(int)
//the time the class starts(double?), instructor name(String), 
// number of students(int)
//if type in a wrong message, type in again

import java.util.Scanner;
	  public class Hw05{
	    //main method required for every Java program
	    public static void main (String[] args){
	      Scanner myScanner = new Scanner(System.in); //for scan
	      String courseNumber; // course number
	      String departmentName; // department name
	      String instructorName; // instructor name
	      int meetTimes;        // meeting times per week
	      int timeStart;     // time to start a class
	      int numStudent;       // number of students 
	      
	      System.out.print("course number: ");     //course number
	    	String junkWord; 
        while(!myScanner.hasNext()){  
           System.out.println("Wrong type ");
           junkWord = myScanner.next();
           System.out.print("course number: ");
        }
	    
	      courseNumber = myScanner.nextLine();
	    // repeat six times while loops
	    
	      System.out.print("department name: ");    // department name
        while(!myScanner.hasNext()){  
           System.out.println("Wrong type ");
           junkWord = myScanner.next();
           System.out.print("department name: ");
        }
	    
	      departmentName = myScanner.nextLine();
	    


	      System.out.print("instructor name: ");  // instructor name 
        while(!myScanner.hasNext()){  
           System.out.println("Wrong type ");
           junkWord = myScanner.next();
           System.out.print("instructor name: ");
        }
	    
	      instructorName = myScanner.nextLine();
 
	      System.out.print("meeting times per week: ");  // instructor name 
        while(!myScanner.hasNextInt()){  
           System.out.println("Wrong type ");
           junkWord = myScanner.next();
           System.out.print("meeting times per week: ");
        }
	      meetTimes = myScanner.nextInt();
	    
        
        // you need to type in two integers 
        // the first integer ranging from 0-23, the second from 00-60
        boolean flag = false;  // if both numbers are out of bound 
        int num = 0; 
        int count = 1; // initilize the first value
        System.out.println("Time to start a class: ");
        while (true) {
            if (myScanner.hasNextInt()) {
                num = myScanner.nextInt();
                if (count == 1) {
                    if (num >= 0 && num < 24) { // range 0-23
                        count = 2;
                    } else {
                        System.out.println("First number out of bound, try again");  
                    }
                } else if (count == 2) { 
                    if (num >= 0 && num <= 60) {  // second range from 00-60
                        flag = true;   
                    } else {
                       System.out.println("Second number out of bound, try again");
                    }
                }
            } else {
                myScanner.next();
            }
            if (flag == true) {
               break;
            }
        }

        
        
        System.out.print("number of students: ");      // number of students 
        while(!myScanner.hasNextInt()){  
           System.out.println("Wrong type ");
           junkWord = myScanner.next();
           System.out.print("number of students: ");
        }
	      numStudent = myScanner.nextInt();
  }
}
