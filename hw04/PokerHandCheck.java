// CSE 2 Hailey Cai, HW04 
// Randomly generate 5 cards 
// one pair = two of the same kind (different suit)
// two pairs = four of the same kind/ ex. JJKK9/33449
// three of a kind = 99983/777J2
// high card hand  = five different kind cards / 8273J
// no loops, methods are allowed
// math random 5 times to generate five random numbers
// to declare 5 string variables -suit1,2,3,4,5 (with initialized value) & cardIdentity1,2,3,4,5;





// import java.util.Random; //
public class PokerHandCheck{
  public static void main(String[] arg){
    
    	//Random myRandom= new Random(); //
		
			int num1 = (int)(Math.random()*52)+1;  // randomly generate five numbers within range 1-52
			int num2 = (int)(Math.random()*52)+1;
			int num3 = (int)(Math.random()*52)+1;
			int num4 = (int)(Math.random()*52)+1;
			int num5 = (int)(Math.random()*52)+1;
			
			String suit1 = "";   // declare a variable suit, with a initialized value, 
			String suit2 = "";
			String suit3 = "";
			String suit4 = "";
			String suit5 = "";
			String cardIdentity1 = "";
      String cardIdentity2 = "";
      String cardIdentity3 = "";
      String cardIdentity4 = "";
      String cardIdentity5 = "";
			
			if(num1<=13) {                      //if number one is smaller than 13, it is Diamond
				suit1 = "Diamonds";	
			}
			else if (num1>=14 && num1<=26) {    // if number one is between 14 to 26, it is Clubs
				suit1 ="Clubs";
			}
			else if (num1>=27 && num1<=39 ) {   // if number one is between 27 to 39, it is Hearts
				 suit1 = "Hearts";
			}
			else if (num1>=40 && num1<= 52) {   // if number one is between 40 to 52, it is Spades
			     suit1 = "Spades";
			}   //num1
			
			
			
			
			if(num2<=13) {              
				suit2 = "Diamonds";	
			}
			else if (num2>=14 && num2<=26) {
				suit2 ="Clubs";
			}
			else if (num2>=27 && num2<=39 ) {
				 suit2 = "Hearts";
			}
			else if (num2>=40 && num2<= 52) {
			     suit2 = "Spades";
			}//num2 (repeat the step from num1)
			
			
			if(num3<=13) {
				suit3 = "Diamonds";	
			}
			else if (num3>=14 && num3<=26) {
				suit3 ="Clubs";
			}
			else if (num3>=27 && num3<=39 ) {
				 suit3 = "Hearts";
			}
			else if (num3>=40 && num3<= 52) {
			     suit3 = "Spades";
			}//num3
			
			if(num4<=13) {
				suit4 = "Diamonds";	
			}
			else if (num4>=14 && num4<=26) {
				suit4 ="Clubs";
			}
			else if (num4>=27 && num4<=39 ) {
				 suit4 = "Hearts";
			}
			else if (num4>=40 && num4<= 52) {
			     suit4 = "Spades";
			}//num4
			
			
			
			if(num5<=13) {
				suit5 = "Diamonds";	
			}
			else if (num5>=14 && num5<=26) {
				suit5 ="Clubs";
			}
			else if (num5>=27 && num5<=39 ) {
				 suit5 = "Hearts";
			}
			else if (num5>=40 && num5<= 52) {
			     suit5 = "Spades";
			}//num5
    
			
			
			int remain1;      //convert 1,11,12,13 to Ace, Jack, Queen and King ; define new variable remain1, divide by 13 and find the remainder; 13 is one suit
			remain1 = num1%13;
			switch(remain1) {   
			
			case 1 :
				cardIdentity1 = "Ace";	// if the remainder is one, (num/13) it is a Ace card, bc 1, 14, 27, 40 are aces and their remainders are all one
				break;		
			case 0: 
				cardIdentity1 = "King";  // if the remainder is zero, (num/13) it is a king (13,26,39,52) these number can all be divided by 13 with no remainder
				break;	
			case 12: 
				cardIdentity1 = "Queen";// if the remainder is twelve, it is a Queen
				break;
			case 11: 
				cardIdentity1 = "Jack"; // if the remainder is 11, it is a Jack 
				break;	
			default:
				cardIdentity1 = ""+remain1; // if the remainder are none of above, use the default case. since cardIdentity is a string , it does not print a int solely, so it requires ""
				break;
			}
			
			int remain2;
			remain2 = num2%13;
			switch(remain2) {
			
			case 1 :
				cardIdentity2 = "Ace";	
				break;		
			case 0: 
				cardIdentity2 = "King";
				break;	
			case 12: 
				cardIdentity2 = "Queen";
				break;
			case 11: 
				cardIdentity2 = "Jack";
				break;	
			default:
				cardIdentity2 = ""+remain2;
				break;
			}  
			
			int remain3;
			remain3 = num3%13;
			switch(remain3) {
			
			case 1 :
				cardIdentity3 = "Ace";	
				break;		
			case 0: 
				cardIdentity3 = "King";
				break;	
			case 12: 
				cardIdentity3 = "Queen";
				break;
			case 11: 
				cardIdentity3 = "Jack";
				break;	
			default:
				cardIdentity3 = ""+remain3;
				break;
			}
			
			int remain4;
			remain4 = num4%13;
			switch(remain4) {
			
			case 1 :
				cardIdentity4 = "Ace";	
				break;		
			case 0: 
				cardIdentity4 = "King";
				break;	
			case 12: 
				cardIdentity4 = "Queen";
				break;
			case 11: 
				cardIdentity4 = "Jack";
				break;	
			default:
				cardIdentity4 = ""+remain4;
				break;
			}

			int remain5;
			remain5 = num5%13;
			switch(remain5) {
			
			case 1 :
				cardIdentity5 = "Ace";	
				break;		
			case 0: 
				cardIdentity5 = "King";
				break;	
			case 12: 
				cardIdentity5 = "Queen";
				break;
			case 11: 
				cardIdentity5 = "Jack";
				break;	
			default:
				cardIdentity5 = ""+remain5;
				break;
			}
			System.out.println("Your random cards were: "); 
			System.out.println("  the " + cardIdentity1 + " of " + suit1);
			System.out.println("  the " + cardIdentity2 + " of " + suit2);
			System.out.println("  the " + cardIdentity3 + " of " + suit3);
			System.out.println("  the " + cardIdentity4 + " of " + suit4);
			System.out.println("  the " + cardIdentity5 + " of " + suit5); // print out five randoly generated numbers 
    
    int t1 =0;
    int t2 =0;
    int t3 =0;
    int t4 =0;
    int t5 =0;
    int t6 =0;
    int t7 =0;
    int t8 =0;
    int t9 =0;
    int t10=0;                // declare new variables 
                                                        // compare two cards, if they equals, return one, else return zero
    t1 = (cardIdentity1.equals(cardIdentity2)) ? 1:0;   // total of 10 outcome by comparing two cards each time 
    t2 = (cardIdentity1.equals(cardIdentity3)) ? 1:0;   // 
    t3 = (cardIdentity1.equals(cardIdentity4)) ? 1:0;
    t4 = (cardIdentity1.equals(cardIdentity5)) ? 1:0;
    t5 = (cardIdentity2.equals(cardIdentity3)) ? 1:0;
    t6 = (cardIdentity2.equals(cardIdentity4)) ? 1:0;
    t7 = (cardIdentity2.equals(cardIdentity5)) ? 1:0;
    t8 = (cardIdentity3.equals(cardIdentity4)) ? 1:0;
    t9 = (cardIdentity3.equals(cardIdentity5)) ? 1:0;
    t10 = (cardIdentity4.equals(cardIdentity5)) ? 1:0;

    int sum = t1+t2+t3+t4+t5+t6+t7+t8+t9+t10;
    System.out.println("");
    switch (sum){
      case 0:
               System.out.println("You have a high card hand!");
           break;
      case 1:
               System.out.println("You have a pair!");
           break;
      case 2:
               System.out.println("You have two pairs");
           break;
      case 3:
               System.out.println("You have three of a kind!");
           break;
      case 4: 
               System.out.println("You have three of a kind!");
           break;
     
        
    }
    
  }
  
  
}