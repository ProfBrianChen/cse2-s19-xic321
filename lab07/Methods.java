// Hailey Cai CSE2
// Mar 22nd, 2019

// basic words needed: 
//adj; non-primary noun for subject, past tense verb, non-primary noun for object



// method 1 : subject- thesis senentence -return subject String
// method 2 : verb -action sentence -refer to SUBJECT in M1 verb
// method 3 : object
// method 4 : adjectives
// method 5 : call on M1-4
// method 6 : conclude 
// main method: 



import java.util.*;

public class Methods{
 
    public static void main(String[] args){
    	 Scanner myScanner = new Scanner(System.in);
    	 String sub = subject();
    	 String vIn = verb();
    	 String adjIn= adjectives();
       String adj2In = adjectives();
       String adj3In = adjectives();
       String oIn = object();
    	 System.out.print("The " +adjIn+ " "+adj2In+" "+sub+" "+vIn+" "+ "the "+adj3In+" "+oIn+".");
    	 //intro sentence with subject
    	 System.out.println();
    	
    	 
    	 Random randomGenerator = new Random();
    	    int sentence = randomGenerator.nextInt(10);
    	    for(int i=0;i<sentence;i++) {
    	    	String completeSentence = combine(sub);
    			System.out.print(completeSentence);
    			System.out.println();
    	    }
    	   
    	    System.out.print("do you want another sentence,'yes' or 'no': ");
    	    String oneMore="yes";
    	   
    	    oneMore= myScanner.nextLine();
    	    
    	    
    	    		while(oneMore.equals("yes")) {
    	    		
    	    			for(int i = 0; i < sentence; i++) {//yes then print another paragraphs 
    	    				String completeSentence = combine(sub);
    	    				System.out.print(completeSentence);
    	    				System.out.println();
    	    			}
    	    			System.out.print("do you want another sentence,'yes' or 'no': ");
	    				oneMore= myScanner.nextLine();
    	    		}
    	
    	    		if (oneMore.equals("no")) {// no, then print conclusion sentence and end it 
    	    			
    	    		    String conclude= conclusion(sub);
    	    			System.out.print(conclude);
    	    			
    	    		}
   
    	    	}
    	    
    	   	// end main :))
    
    
    
    public static String combine(String s) {// combine all the words to a sentence with changing subjects
//    	
    	Random randomGenerator = new Random();
   	    int sentence = randomGenerator.nextInt(3);
//   
   	   
  //	   String s = subject();
//     String v = verb();
//     String adj= adjectives();
//     String adj2 = adjectives();
//     String adj3 = adjectives();
//     String o = object();
	 //  System.out.print("The " +adj+ " "+adj2+" "+s+" "+v+" "+ "the "+adj3+" "+o);
	   String temp=" ";
    	
    		switch(sentence){
    			case 0: temp="The " +adjectives()+ " "+adjectives()+" "+s+" "+verb()+" "+ "the "+adjectives()+" "+object()+".";
    			break;
    			
    			case 1: temp="This "+s+" "+"was "+verb()+ " to "+adjectives()+" "+object()+".";
    			break;
    			
    			case 2: temp="It used "+ object()+" to "+verb()+" at the "+adjectives()+" "+s+".";
    			break;
    			
    			case 3: temp="And "+s+ " is " +adjectives()+ " about the "+ object()+".";
    			break;
    			
    			default: temp = "Error";
    			break;
    	}
    	 
    	return temp;
    }

    public static String subject(){ //method one return subject
        Random randomGenerator = new Random();
        String temp1=" ";
        int num = randomGenerator.nextInt(10);  // it randomly generates number less than 10
        switch(num){
            case 0: temp1="Cat";
                break;
            case 1: temp1="Dog";
                break;
            case 2: temp1="Fish";
                break;
            case 3: temp1="Mouse";
                break;
            case 4: temp1="Butterfly";
                break;
            case 5: temp1="Dragon";
                break;
            case 6: temp1="Bird";
                break;
            case 7: temp1="Lion";
                break;
            case 8: temp1="Bear";
                break;
            case 9: temp1="Mammal";
                break;
            default: temp1="Error";
                break;
        }

       return temp1;
    }

    public static String verb(){//method2: input verbs
        Random randomGenerator = new Random();
        int num = randomGenerator.nextInt(10);
        String temp2=" ";

        switch(num){
            case 0: temp2="eats";
                break;
            case 1: temp2="takes";
                break;
            case 2: temp2="makes";
                break;
            case 3: temp2="puts";
                break;
            case 4: temp2="runs";
                break;
            case 5: temp2="thinks";
                break;
            case 6: temp2="enables";
                break;
            case 7: temp2="finds";
                break;
            case 8: temp2="becomes";
                break;
            case 9: temp2="calls";
                break;
            default: temp2="Error";
                break;
        }
        return temp2;
    }

    public static String adjectives(){ // method 3 : adjectives
        Random randomGenerator = new Random();
        int num = randomGenerator.nextInt(10);
        String temp3="";

        switch(num){
            case 0: temp3="beautiful";
                break;
            case 1: temp3="agreeable";
                break;
            case 2: temp3="marvelous";
                break;
            case 3: temp3="fantasic";
                break;
            case 4: temp3="angry";
                break;
            case 5: temp3="pitiful";
                break;
            case 6: temp3="gigantic";
                break;
            case 7: temp3="plain";
                break;
            case 8: temp3="clumsy";
                break;
            case 9: temp3="witty";
                break;
            default: temp3="Error";
                break;
        }
        return temp3;
    }


    public static String object(){ // method 4 : object
        Random randomGenerator = new Random();
        int num = randomGenerator.nextInt(10);
        String temp4="";

        switch(num){
            case 0: temp4="house";
                break;
            case 1: temp4="homework";
                break;
            case 2: temp4="school";
                break;
            case 3: temp4="Java Program";
                break;
            case 4: temp4="church";
                break;
            case 5: temp4="alcohol";
                break;
            case 6: temp4="chemistry";
                break;
            case 7: temp4="ice-cream";
                break;
            case 8: temp4="spaghetti";
                break;
            case 9: temp4="toothpaste";
                break;
            default: temp4="gymastics";
                break;
        }
        return temp4;
    }
    public static String conclusion(String sss) {
        Random randomGenerator = new Random();
        int num = randomGenerator.nextInt(2);
        
        String temp =" ";
        switch(num) {
        case 0: temp="The "+sss+" "+ verb()+"her "+object()+"!";
        break;
        case 1: temp ="The "+sss+" "+ verb()+ "him "+object()+"!";
        break;
        default: temp="error";
        break;
        }
        
        
      
        return temp;
    }

  
  
  
  
  
  
}