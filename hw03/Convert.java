//CSE2 Hailey Cai Feb 10th, 2019
//convert meters to inches
//use Scanner class




import java.util.Scanner; // import Scanner Classs
public class Convert{
    public static void main(String[] arg){ //main method
     Scanner myScanner = new Scanner(System.in); //construct
     for(int i=0; i<2;i++){       //for loop, repeat the steps twice
     System.out.print("Enter the distance in meters: "); //print "enter the distance in meters"
     double meters = myScanner.nextDouble(); //use the scanner class
	 	 double inches=(39.3700787*meters); //convert meters to inches
     System.out.printf(meters + " meters is %4.4f ", inches); //use printf(%f) to round to 4 decimal place
     System.out.println("inches."); 
     System.out.println(" "); //space 
        
     }
    
    }
}