
//find the volume of box 
//use scanner class to print  width/ length/height 
//apply volume formula 
//convert double to int with no decimal at the end




import java.util.Scanner; // import Scanner class
public class BoxVolume{
  public static void main(String arg[]){ //main method 
    Scanner myScanner = new Scanner(System.in); //declare class
    System.out.print("The width side of the box is "); //print 
    double width = myScanner.nextDouble(); //value width on screen
    System.out.print("The length of the box is ");
    double length = myScanner.nextDouble();
    System.out.print("The height of the box is ");
    double height = myScanner.nextDouble();
    
    double volume = height * length*width; //volume formula 
    int a = (int)volume; //convert volume(with decimal) to an integer 
    
    System.out.print("The volume inside the box is " + a);

  }
}