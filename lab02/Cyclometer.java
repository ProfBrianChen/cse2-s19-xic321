//Hailey Cai, 02/01/2019, CSE02
//Cyclometer: measure 1. the time elapsed in seconds; 2. the # of rotations of the front wheel during the time. 
//print the # of minutes of minutes for each trip 
//print the # of counts for each trip 
//print the distance of each trip in miles 
//print the distance for the two trips combined 

public class Cyclometer{
      // main method required for every Java program
      public static void main(String[] args){
     // our input data.       
        int secsTrip1=480;  // trip 1 takes 480 seconds
          int secsTrip2=3220; // trips 2 takes 3220 seconds 
        int countsTrip1=1561; // the front wheel rotates 1561 times during trip one
        int countsTrip2=9037; // the front wheel rotates 9037 times during trip two
        double wheelDiameter=27.0, //this the diameter of the wheel
               PI = 3.14159, // formula
               feetPerMile= 5280, // change in unit
               inchesPerFoot = 12, // change in unit 
               secondsPerMinute =60;  // change in unit 
        double distanceTrip1, distanceTrip2, totalDistance;//store variable distance for trip 1, trip2.double
        
        System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
        System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
      // distanceTrip1 = countsTrip1*wheelDiameter*PI;
      
        
               
      
        
        
      }   //end of main method 
}     // end of class